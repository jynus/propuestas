---
layout: 2022/post
section: proposals
category: devrooms
author: Fedora
title: Fedora Hatch meetup
---

# Fedora Hatch meetup

## Detalles de la propuesta:

-   Descripción:

> Meetup de la comunidad de Fedora tanto para usuarios o contribuidores de Fedora o personas que esten interesadas en conocer mas sobre la comunidad y nuestros proyectos. Charlaremos sobre como puede mejorar la comunidad, la nueva release de Fedora 36, Fedora CoreOS y otros temas relacionados con la comunidad.<br><br>
Esta sala también ayudará a personas nuevas a introducirse a la comunidad, siendo al mismo tiempo una buena oportunidad para reunir a la comunidad española.

-   Formato:

> Meetup de 2 horas de duracion, nos sentaremos y charlaremos de forma comoda y abierta de los temas que nos interese sobre la comunidad. Se incluiran una serie de actividades iniciales para romper el hielo.

-   Público objetivo:

> Cualquier asistente de esLibre.

## Comunidad que propone la sala:

-   Nombre: Fedora

-   Info:

> La comunidad de Fedora gira entorno a los diferentes proyectos de Fedora relacionado con GNU/Linux como las distribuciones Fedora Workstation, Fedora Server, Fedora CoreOS..<br><br>
Además de todos los programa que Fedora organiza o participa en ellos como el Outreachy.

-   Web: <https://getfedora.org/es/>
-   Twitter: <https://twitter.com/fedora>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso
