---
layout: 2022/post
section: proposals
category: talks
author: Luis Fajardo
title: Las ventajas de usar F-Droid frente a Google Play&#58 Un par de Apps imprescindibles para la privacidad.
---

# Las ventajas de usar F-Droid frente a Google Play: Un par de Apps imprescindibles para la privacidad.

> Se expondrán las ventajas de usar software auditado en términos de libertad, detallando paso a paso como habilitar un repositorio para instalar software libre en Android. Realizaremos un ejemplo de instalación de la alternativa libre a WhatsApp en Android, Conversations, permitiendo que los asistentes y oyentes salgan con la red ejabber impulsada por la asociación EDUCATIC (TXS.es, tecnologías para la sociedad) plenamente operativa para chat de texto y multimedia, audio y vídeollamadas.<br><br>
Se verá así de una forma práctica la conveniencia de abrir los ojos (y el teléfono) a un mundo de aplicaciones no controladas por los señores del capitalismo salvaje de la vigilancia, las empresas que comercializan con tus datos con cada instalación que realizas. El software contenido en el repositorio de F-Droid es una alternativa a ese. Mostraremos sus contenidos principales y más prácticos para el día a día.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> Se expondrán las ventajas de usar software auditado en términos de libertad, detallando paso a paso como habilitar un repositorio para instalar software libre en Android. Realizaremos un ejemplo de instalación de la alternativa libre a WhatsApp en Android, Conversations, permitiendo que los asistentes y oyentes salgan con la red ejabber impulsada por la asociación EDUCATIC (TXS.es, tecnologías para la sociedad) plenamente operativa para chat de texto y multimedia, audio y vídeollamadas.<br><br>
Se verá así de una forma práctica la conveniencia de abrir los ojos (y el teléfono) a un mundo de aplicaciones no controladas por los señores del capitalismo salvaje de la vigilancia, las empresas que comercializan con tus datos con cada instalación que realizas. El software contenido en el repositorio de F-Droid es una alternativa a ese. Mostraremos sus contenidos principales y más prácticos para el día a día. Detalladamente:
   -   Métodos de "liberar tu móvil" e importancia de hacerlo.
   -   Valor de la opción elegida para esta charla.
   -   Instalando F-Droid...
   -   Conversations. Usando jabber desde Android.
   -   Otros clientes.
   -   Principales Apps para evitar el uso de clientes no libres.

-   Web del proyecto: <https://educatic.txs.es>

-   Público objetivo:

> Todos. Divulgación general. Usuarios de móviles, en ámbito privado y profesional (cumplimiento RGPD). Usuarios de teléfonos inteligentes y tablets preocupados por su privacidad.

## Ponente:

-   Nombre: Luis Fajardo

-   Bio:

> Profesor de Derecho civil en la ULL, siempre vinculado a la defensa de las libertades en el ciberespacio. Dr en Derecho por la UAM, comienza impartiendo clases en esa Universidad, y luego en la UdG, UNED y ULL. Ha sido Abogado, Juez, y DPD. Socio fundador de EDUCATIC, y administrador de varios de sus sistemas, desde donde se impulsa el proyecto Tecnología para la Sociedad (txs.es), que pretende convertirse en Fundación.

### Info personal:

-   Web personal: <https://encanarias.info/people/56066f30d1620135a1394b44294f2d2e>
-   Mastodon (u otras redes sociales libres): <https://txs.es/@lfajardo>
-   Twitter: <https://twitter.com/lfajardo>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://gitlab.com/luisfa>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
