---
layout: 2022/post
section: proposals
category: talks
author: Italo Vignoli
title: Open Source and Open Standards for Digital Sovereignty
---

# Open Source and Open Standards for Digital Sovereignty

> Open Source Software and Open Standards – especially document formats – are of key importance for the digital sovereignty strategies of individuals, companies, organisations and governments. Today, user-created content - and the ability to share it transparently - is in the hands of a few companies, which exploit the limited digital culture of users to their advantage. This situation can only be overcome by moving from proprietary to open source software and from proprietary to open standards.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Remota

-   Descripción:

> Open Source Software and Open Standards – especially document formats – are of key importance for the digital sovereignty strategies of individuals, companies, organisations and governments. Today, user-created content - and the ability to share it transparently - is in the hands of a few companies, which exploit the limited digital culture of users to their advantage. This situation can only be overcome by moving from proprietary to open source software and from proprietary to open standards.

-   Web del proyecto: <https://www.libreoffice.org>

-   Público objetivo:

> Open source and open standards advocates

## Ponente:

-   Nombre: Italo Vignoli

-   Bio:

> Italo Vignoli is a founding member of The Document Foundation and the LibreOffice project, the Chairman Emeritus of Associazione LibreItalia, an Ambassador of Software Heritage, and a proud member of Free Software Foundation Europe (FSFE). He is a past board member of Open Source Initiative (OSI).<br><br>
Italo co-leads LibreOffice marketing, PR and media relations, co-chairs the LibreOffice Certification Program, and is a spokesman for the project. He also handles advocacy and marketing activities for the Open Document Format ISO standard.

### Info personal:

-   Web personal: <https://www.vignoli.org>
-   Mastodon (u otras redes sociales libres): <https://fosstodon.org/@italovignoli>
-   Twitter: <https://twitter.com/@italovignoli>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
