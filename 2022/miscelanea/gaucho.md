---
layout: 2022/post
section: proposals
category: misc
author: angrykoala
title: Gaucho
---

# Gaucho

> Gaucho es un lanzador de scripts y tareas que permite configurar comandos para ejecutarlos desde una interfaz gráfica, sin necesidad de múltiples terminales para tareas comunes.

## Detalles de la propuesta:

-   Tipo de propuesta: Tablón de proyectos

-   Descripción:

> Gaucho es un lanzador de scripts y tareas que permite configurar comandos para ejecutarlos desde una interfaz gráfica, sin necesidad de múltiples terminales para tareas comunes.<br><br>
Entre otras funcionalidad, Gaucho permite organizar y configurar tareas para lanzar en Windows, Mac y Linux. Gestionar variables de entorno y programar ejecuciones para lanzar tareas automaticamente

-   Web del proyecto: <https://angrykoala.github.io/gaucho/>

-   Público objetivo:

> Desarrolladores, sysadmins y usuarios avanzados

## Ponente:

-   Nombre: angrykoala

### Info personal:

-   Web personal: <https://github.com/angrykoala>
-   Twitter: <https://twitter.com/the_angry_koala>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/angrykoala>


## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
