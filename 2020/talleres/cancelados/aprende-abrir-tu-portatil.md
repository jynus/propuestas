---
layout: 2020/post
section: proposals
category: workshops
title: Aprende a abrir tu portátil y cambiale la pasta térmica
---

En este taller práctico puedes venir con tu portátil, y abrirlo si quieres. Carlos, Javier y Alejandro (confía en ellos, son miembros del equipo técnico de Slimbook), con mucha experiencia en apertura de portátiles, te indicarán como hacerle el mantenimiento a tu ordenador, sea de la marca que sea.

Nosotros podremos los destornilladores de precisión, el aire comprimido y la pasta térmica de primera calidad (hasta un máximo de 20 asistentes).

Si por el contrario quieres venir sin tu ordenador, sólo quieres ver el interior de los ordenadores, contaremos con 4 máquinas, para que realices el taller con ellos.  

## Objetivos a cubrir en el taller

El objetivo de este taller es dotar a los asistentes de los conocimientos básicos para la apertura de us ordenador portátil, consiguiendo con ello la limpieza interior y el cambio de la pásta térmica. Una práctica que debería realizarse en todos los ordenadores portátiles cada 2 años.

## Público objetivo

Este taller va dirigido a gente que no suela trabajar desmontando portátiles, pero no se considere torpe con los destornilladores, y que en general le guste el cacharreo :)

## Ponente(s)

Carlos Pons, Javier Jareño, y Alejandro López son parte del equipo técnico que trabajan para Slimbook.

### Contacto(s)

-   Alejandro López: alejandrolopez at slimbook dot es

## Prerrequisitos para los asistentes

Conocimientos básicos de hardware y cierta soltura en el manejo del destornillador.

## Prerrequisitos para la organización

Una sala con mesas, para un máximo de 20 personas, pero con pasillos relativamente amplios para que los técnicos puedan colocarse durante largos momentos junto a los asistentes, para ayudarles a desmontar sus ordenadores.

## Tiempo

2 horas

## Día

Viernes o sábado, a confirmar por no coincidencia de otra charla

## Comentarios

Viva la comunidad.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/).
-   [x]  Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
-   [x]  Acepto coordinarme con la organización de esLibre.
