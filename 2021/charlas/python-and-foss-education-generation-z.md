---
layout: 2021/post
section: proposals
category: talks
author: Gajendra Deshpande
title: Python and FOSS in Education for Generation Z
---

Are you interested to learn how to teach Generation Z or post-millennials using modern tools? Then you have come to the right place. I will talk about using the smart-phone as a learning device and tools such as QPython, Blockly, Flowgorithm, Code2flow, PyCallgraph and Jupyter command line.

## Proposal format

Indicate one of these:
-   [ ]  Short talk (10 minutes)
-   [x]  Talk (25 minutes)

## Description

The students who belong to Generation Z or post-millennials have access to gadgets such as smartphones even before they go to school. This makes them technology savvy and at the same time, they get bored easily in a traditional classroom setting. It becomes necessary to use modern tools and techniques in the classroom to engage students in activities. Also, governments are promoting the “Bring Your Own Device (BYOD)” concept in education which can be a boon to those who cannot afford a computer or laptop.

In this talk, I will introduce the QPython which can be installed on a smartphone to execute Python programs. I will also introduce visual programming tools like Google Blockly,  and flowgorithm to generate Python code. Next, I will discuss a friendly package to generate developer-friendly error messages. Next, I will demonstrate code visualization packages in Python such as code2flow and pycallgraph. Finally, I will discuss conducting science experiments on the go using smartphone.

Outline:

1. Who are Gen Z? and their characteristics (02 Minutes)
2. Next Gen teacher (02 Minute)
3. QPython (04 Minutes)
4. Debugging Python programs using friendly (05 Minutes)
5. Flowgorithm (05 Minutes)
6. Blockly (02 Minutes)
7. Code2flow and PyCallgraph(03 Minutes)
8. Jupyter Command line (02 Minutes)
9. Conducting experiments on the go using smartphone (05 Minutes)

-   Project website:

## Target audiences

Most suitable for students, teachers, and open source contributors in the education field.

## Speaker/s

I hold M.Tech. in Computer Science and Engineering and PG Diploma in Cyber Law and Cyber Forensics from National Law School of India University, Bengaluru India.

I have presented talks/posters/papers at prestigious conferences including JuliaCon, London, PyCon France, PyCon Hong Kong, PyCon Taiwan, COSCUP Taiwan, PyCon Africa, BuzzConf Argentina, EuroPython, PiterPy Russia, SciPy USA, SciPy India, NIT Goa, and IIT Gandhi Nagar.

Worked as a Reviewer for many reputed International conferences.

### Contact/s

-   Name: Gajendra Deshpande
-   Email: <gcdeshpande@hotmail.com>
-   Personal website: <https://gcdeshpande.github.io>
-   Mastodon (or other free social networks):
-   Twitter: <https://twitter.com/gcdeshpande>
-   GitLab:
-   Portfolio or GitHub (or other collaborative code sites): <https://github.com/gcdeshpande>

## Comments
